---
layout: page
title: "PARS-Workshop"
description: "29. Workshop der GI/ITG-Fachgruppe PARS"
header-img: "img/aachen.jpg"
---

![Logis](/img/logos.png)

## 29. Workshop der GI/ITG-Fachgruppe "Parallel-Algorithmen, -Rechnerstrukturen und -Systemsoftware" (PARS) am 14.-15. September 2023, RWTH Aachen

### Hintergrund

Ziel des PARS-Workshops ist die Vorstellung wesentlicher Aktivitäten im Arbeitsbereich von PARS und ein damit verbundener Gedankenaustausch. Mögliche Themenbereiche sind:

* Parallele Algorithmen (Beschreibung, Komplexität, Anwendungen)
* Parallele Rechenmodelle und parallele Architekturen
* Parallele Programmiersprachen und Bibliotheken
* Werkzeuge der Parallelisierung (Compiler, Leistungsanalyse, Auto-Tuner)
* Parallele eingebettete Systeme / Cyber-Physical Systems
* Software Engineering für parallele und verteilte Systeme
* Multicore-, Manycore- und heterogene Architekturen (inkl. Beschleuniger, wie GPUs und FPGAs)
* Cluster und Cloud Computing
* Technologien für paralleles Rechnen (u.a. Netzwerk-, und Speichertechnologien; Quantencomputing etc.)
* Parallelverarbeitung in der Lehre (Erfahrungen, E-Learning)
* Methoden des parallelen und verteilten Rechnens in der Praxis

Die Sprache des Workshops ist Deutsch und Englisch. Einreichungen erfolgen wahlweise in Form eines maximal einseitigen Abstracts (**nur** Vortrag) oder als 4- bis max. 10-seitiges bislang unpubliziertes Papier. Über die Annahme aller Einreichungen entscheidet ein Programmkomitee. Die unpublizierten Papiere werden als PARS-Mitteilungen (ISSN 0177-0454) und digital bei der GI publiziert. 

### Veranstaltungsort

Der Workshop findet im Raum 00.24 (Hauptgebäude) des [E.ON Energy Research Centers der RWTH Aachen](https://maps.rwth-aachen.de/navigator/?lang=de&type=hybrid&obj=4120) statt. Eine Wegbeschreibung ist [hier](https://pars.pages.rwth-aachen.de/pdf/anfahrtsskizze.pdf) zu finden. Für Bahnkunden empfiehlt es sich am West-Bahnhof Aachen auszusteigen. GI-Mitglieder können [vergünstigte Bahnreisen](https://gi.de/bahn) buchen, wenn sie GI-Veranstaltungen besuchen.

### Anmeldung

Die Anmeldung kann durch eine E-Mail an [Dr. Stefan Lankes](mailto:slankes@eonerc.rwth-aachen.de) erfolgen. Anschließend erhalten Sie eine Zahlungsaufforderung. Die Teilnahmegebühr wird 90€ betragen.

### Programm für den 14. September 2023

* ab 12:00 Registrierung und Imbiss

**Session 1: Begrüßung und Keynote** (Chair: W. Karl)

* 12:40 – 12:45: Begrüßung
* 12:45 – 13:30: Norbert Eicker, *[Modular Supercomputing Architecture (MSA) to Exascale in Europe - Research on Parallel Architectures at JSC](../pdf/slides/eicker.pdf)*

**Session 2: IO and Compression** (Chair: P. Sobe)

* 13:30 – 14:00: Jannek Squar, Anna Fuchs, Michael Kuhn and Thomas Ludwig, *[Automatic Code Transformation of netCDF Code for I/O Optimisation](../pdf/slides/squar.pdf)*
* 14:00 – 14:30: Marco Vogel and Lena Oden, *[Evaluation of GPU-Compression Algorithms for CUDA-Aware MPI](../pdf/slides/vogel.pdf)*

**Kaffeepause**

**Session 3: Modelling and Benchmarking of MPI** (Chair: B. Schnor)

* 15:00 – 15:30: Kronje Krabbe, Michael Blesel and Michael Kuhn, *[Modelling MPI Communication using Coloured Petri Nets](../pdf/slides/blesel.pdf)*
* 15:30 – 16:00: Laura Fuentes Grau, Niklas Eiling, Stefan Lankes, Antonello Monti, *[TosKonnect - A Modular Queue-based Communication Layer for Heterogeneous High Performance Computing](../pdf/slides/fuentes_grau.pdf)*
* 16:00 – 16:30: Philipp Gschwandtner, *Beyond Benchmarks: Comparing Parallel Programming Models in Real-World Scenarios*

**Kaffeepause**

**Session 4: Sitzung des Leitungsgremiums**

**Gemeinsames Abendessen**

* 20:00: [Aposto Aachen](https://aachen.aposto.eu)

### Programm für den 15. September 2023

**Session 5: Energieeffiziente Rechenzentren** (Chair: S. Christgau)

* 9:00 – 9:45: Führung durch die Hallen des Lehrstuhls für Gebäude- und Raumklimatechnik
* 9:45 – 10:00: Christoph von Oy, Stefan Lankes, *Betriebsoptimierung energieeffizienter Rechenzentren durch die Verwendung digitaler Zwillinge*

**Kaffeepause**

**Session 6: Heterogeneous Computing** (Chair: L. Oden)

* 10:30 – 11:00: Peter Sobe, *[Representing Execution Variations in Parallel Communication Protocols](../pdf/slides/sobe.pdf)*
* 11:00 – 11:30: Viktor Skoblin, Felix Höfling, Steffen Christgau, *[Gaining Cross-Platform Parallelism for HAL’s Molecular Dynamics Package using SYCL](../pdf/slides/skoblin.pdf)*
* 11:30 – 12:00: Roman Lehmann, Paul Schaarschmidt, Wolfgang Karl, *[Comparing GPU and TPU in an Iterative Scenario: A Study on Neural Network-based Image Generation](../pdf/slides/lehmann.pdf)*

**Kaffeepause**

**Session 7: Network-Attached Accelerators** (Chair: C. Clauß)

* 12:30 – 13:00: Steffen Christgau, Dylan Everingham, Tobias Jaeuthe, Marco de Lucia, Max Lübke, Florian Mikolajczak, Danny Puhan, Niklas Schelten, Bettina Schnor, Johannes Spazier, Benno Stabernack and Fritjof Steinert, *[NAAICE - Network-Attached Accelerators in Heterogenous Computing Environments](../pdf/slides/schnor_christgau.pdf)*
* 13:00 – 13:00: Frederic Schimmelpfenning, *ScalNEXT – Optimierung des Datenmanagements und des Kontrollflusses von Rechenknoten*
* 13:30: Verabschiedung
 
### Termine und Rahmenbedingungen

* Einreichungsfrist für Beiträge **(verlängert!)**: **4. August 2023** ~~**25. Juli 2023**~~
* Abstracts und Beiträge im Umfang von 4-10 Seiten (Format: [GI Lecture Notes in Informatics](https://gi.de/service/publikationen/lni/), nicht vor-veröffentlicht) sind in elektronischer Form unter folgendem Link einzureichen: [https://easychair.org/conferences/?conf=pars23](https://easychair.org/conferences/?conf=pars23)
* Benachrichtigung der Autoren bis 25. August 2023
* Druckfertige Ausarbeitungen bis 15. Oktober 2023 ~~29. September 2023~~ (nach dem Workshop)

### Programmkomitee

* Steffen Christgau (Berlin)
* Carsten Clauss (Jülich)
* Andreas Döring (Zürich)
* Norbert Eicker (Jülich)
* Diana Göhringer (Dresden)
* Wolfgang Karl (Karlsruhe)
* Jörg Keller (Hagen)
* Michael Kuhn (Magdeburg)
* Stefan Lankes (Aachen)
* Erik Maehle (Lübeck)
* Ulrich Margull (Ingolstadt)
* Richard Membarth (Ingolstadt)
* Lena Oden (Hagen)
* Bettina Schnor (Potsdam)
* Martin Schulz (München)
* Peter Sobe (Dresden)
* Benno Stabernack (Berlin)
* Estela Suarez (Bonn)
* Carsten Trinitis (München)
* Rolf Wanka (Erlangen)

### Nachwuchspreis

Der beste Beitrag, der auf einer Diplom-/Masterarbeit oder Dissertation basiert, und von dem Autor/der Autorin selbst vorgetragen wird, wird auf dem Workshop von der Fachgruppe PARS mit einem Preis (dotiert mit 500 €) ausgezeichnet. Co-Autoren sind erlaubt, der Doktorgrad sollte zum Zeitpunkt der Einreichung noch nicht verliehen sein. Die Bewerbung um den Preis erfolgt durch E-Mail an die Organisatoren bei Einreichung des Beitrages. 

### Veranstalter

* [GI/ITG-Fachgruppe PARS](https://fg-pars.gi.de)

### Organisation

* Dr. Stefan Lankes, RWTH Aachen, Lehrstuhl für Automation of Complex Power Systems, Mathieustraße 10, 52074 Aachen Germany, Tel.: (0241) 80 49740, E-Mail: slankes@eonerc.rwth-aachen.de
* Dr. Steffen Christgau, Zuse-Institut Berlin, Abteilung Supercomputing, Takustraße 7, 14195 Berlin, Germany, Tel.: (030) 84185-214, E-Mail: christgau@zib.de 
* Prof. Dr. Wolfgang Karl (PARS-Sprecher), Karlsruher Institut für Technologie, Rechnerarchitektur und Parallelverarbeitung, 76131 Karlsruhe, Germany, Tel.: (0721) 608-43771, E-Mail: karl@kit.edu

### Bildrechte

* Photo by ChristosV [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0), via Wikimedia Commons
